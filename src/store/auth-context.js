import React, {useEffect, useState, useCallback} from 'react';

let logoutTimer;

const AuthContext = React.createContext({
    token: '',
    isLoggedIn: false,
    login: (token) => {},
    logout:() => {}
});

const calculateRemainingTime = (expirationTimeString) => {
    const currentTime = new Date().getTime();
    const expirationTimeMS = new Date(expirationTimeString).getTime();
    return expirationTimeMS - currentTime;
};

const getStoredToken = () => {
    const storedExpirationDate = localStorage.getItem('expirationTime');
    const remainingTime = calculateRemainingTime(storedExpirationDate);
    const isTokenValid = remainingTime > 3600;
    if(!isTokenValid) {
        localStorage.removeItem('token');
        localStorage.removeItem('expirationTime');
        return null;
    }
    return {
        token: localStorage.getItem('token'),
        duration: remainingTime
    };
}
export const AuthContextProvider = (props) => {
    const tokenData = getStoredToken();
    let initialToken;
    if(tokenData)
    {
        initialToken = tokenData.token;
    }
    const [token, setToken] = useState(initialToken);
    const isUserLoggedIn = Boolean(token);

    const onLogout = useCallback(() => {
        setToken(null);
        localStorage.removeItem('token');
        localStorage.removeItem('expirationTime');
        if(logoutTimer) {
            clearTimeout(logoutTimer);
        }
    },[]);

    const onLogin = (token, expirationTime) => {
        setToken(token);
        localStorage.setItem('token',token);
        localStorage.setItem('expirationTime',expirationTime);
        const remainingSessionTime = calculateRemainingTime(expirationTime);

        logoutTimer = setTimeout(onLogout, remainingSessionTime);
    };

    useEffect(()=>{
        if(tokenData) {
            console.log("tokenData.duration",tokenData.duration)
            logoutTimer = setTimeout(onLogout, tokenData.duration);
        }
    },[tokenData,onLogout]);

    const contextValue = {
        token,
        isLoggedIn: isUserLoggedIn,
        login: onLogin,
        logout: onLogout
    };

    return <AuthContext.Provider value={contextValue}>
        {props.children}
    </AuthContext.Provider>
};

export default AuthContext;